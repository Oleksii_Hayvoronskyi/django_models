from django.urls import path
from . import views

urlpatterns = [
    path('smoke_tobacco/', views.smoke_tobacco, name='tobacco'),
    path('create_client/', views.create_client, name='client'),
    path('buy_tobacco/', views.buy_tobacco, name='buy'),
]