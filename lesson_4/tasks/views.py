from django.shortcuts import render
from decimal import Decimal
from uuid import uuid4
from django.http import HttpResponse
from tasks.models import Tobacco, Set_of_tobacco, Client
from django.contrib.auth.models import User
from django.core.files import File


def smoke_tobacco(request):
    fumari = Tobacco()
    fumari.count = 5
    fumari.description = 'Табак для кальяна Fumari выпускается в США, ' \
                         'но за счет отличного качества и обширного ' \
                         'ассортимента вкусов, стал популярен и во всем мире. ' \
                         'Производитель начал изготовлять данный муассель ' \
                         'в 1997 году, и уже сегодня он относится к сегменту' \
                         ' премиум-класса.'
    fumari.could_I_try_it = True
    fumari.wiki_page = 'https://hardsmoke.shop/shop/tabak-fumari.html'
    # fumari.name = 'Fumari'
    # Зберігає у базі даних.
    fumari.save()
    return HttpResponse("Let's smoke!")


# Створюю клієнта.
def create_client(request):
    # # Відкриє файл для читання.
    # with open('invoice.txt', 'r') as _file:
    #     tmp_file = _file.read()
    # Задаю поля клієнта, які зазначені для клієнта в models.py.
    client = Client.objects.create(**{
        'user': User.objects.get(pk=1),
        'second_email': 'admin@admin1.com',
        'name': "Valera",
        # 'invoice': File(open('tmp/invoice.txt')), #!!! Не знаходить шлях!Требв розібратися
        'user_uuid': uuid4(),
        'discount_size': Decimal('0.00256'),
        'client_ip': '192.0.2.1',
    })
    return HttpResponse(client)


def buy_tobacco(requests):
    # Витягнути з бази даних конкретний об’єтк.
    price = Set_of_tobacco.shop.get(id=2).price
    # Відправляю на frontend.
    return HttpResponse(price)


