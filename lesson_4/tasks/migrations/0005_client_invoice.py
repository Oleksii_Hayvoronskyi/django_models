# Generated by Django 4.1.2 on 2022-10-26 22:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0004_remove_client_invoice'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='invoice',
            field=models.FileField(null=True, upload_to=''),
        ),
    ]
