from django.db import models
from datetime import datetime, timedelta
from django.contrib.auth.models import User


# Створюю модель для мокальян-крамниці.
class Tobacco(models.Model):
    # Додаю поля з параметрами.
    count = models.IntegerField(default=0, blank=True, null=True)
    description = models.TextField(null=True)
    delivered_at = models.DateTimeField(auto_now_add=True, blank=True,
                                        null=True)
    could_I_try_it = models.BooleanField(default=True, null=True)
    # Посилання на опис товару.
    wiki_page = models.URLField(default='https://uk.wikipedia.org/',
                                # Комбінація поля -> delivered_at (into Tobacco)
                                # та цієї дати має юути унікальною.
                                name='wikipedia',
                                unique_for_date='delivered_at', null=True)
    # Назва табака для кальяна.
    name = models.CharField(max_length=64, unique=True, null=True)


# Модель набору табаку з різними смаками
class Set_of_tobacco(models.Model):
    # Створив менеджера.
    shop = models.Manager()
    # Скільки часу табак буде залишатися димним і смачним.
    smoking_period = models.DurationField(default=timedelta(minutes=40),
                                          help_text='Info about how long you '
                                                    'can smoke this tobacco',
                                          null=True)
    photo = models.ImageField(blank=True, null=True)
    price = models.FloatField(default=1.0, null=True)
    tobacco = models.ManyToManyField(Tobacco, verbose_name='This set include'
                                                           ' this tobacco')


# Модель замовника (клієнта).
class Client(models.Model):
    # При видаленні користувача, всі пов’язані з ним елементи будуть каскадно видалятися.
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    second_email = models.EmailField(null=True)
    name = models.CharField(max_length=64, null=True)
    # invoice = models.FileField(null=True, upload_to='uploads/%Y/%m/%d/') # !!! Не знаходить шлях!Требв розібратися
    # Означає (editable), що це поле не буде змінюватися.
    user_uuid = models.UUIDField(editable=False, null=True)
                                        # Кількість цифр до (5) та після (2) коми.
    discount_size = models.DecimalField(max_digits=5, decimal_places=2,
                                        null=True)
    client_ip = models.GenericIPAddressField(blank=True, null=True,
                                             protocol='IPv4')
